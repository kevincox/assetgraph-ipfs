import AssetGraph from "assetgraph";
import crypto from "crypto";
import importer from "ipfs-unixfs-importer";

// https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/crossorigin
const CROSSORIGIN_ELEMENTS = /AUDIO|IMG|LINK|SCRIPT|VIDEO/;

// https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity
const INTEGRITY_ELEMENTS = /LINK|SCRIPT/;

// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy#integration_with_html
const REFERRERPOLICY_ELEMENTS = /a|area|iframe|img|link|script/;

/** Update assets to be fetched via an IPFS gateway.
 *
 * See the README.md for full docs.
 *
 * @param assets {Asset[]} A list of assets to transform.
 * @param options {Object} See the README for options.
 * @returns {Object[]} An array with an element for each passed in asset. Each element will contain the following.
 *   cid: The CID of the asset. See https://github.com/multiformats/js-cid.
 *   gatewayAsset: The new asset representing the gateway URL.
 */
export async function useGateway(
	assets,
	{
		crossorigin = "",
		gatewayHost = "https://cloudflare-ipfs.com",
		gatewayUrl = null,
		ipfsOptions = {},
		referrerPolicy = null,
		rename = true,
		sri = "sha256",
	} = {},
) {
	if (!gatewayUrl) {
		gatewayUrl = ({cid, path}) => `${gatewayHost}/ipfs/${cid.toV0()}/${path}`;
	}

	ipfsOptions = {onlyHash: true, ...ipfsOptions};

	let dependencies = {};
	for (let a of assets) {
		let r = dependencies[a.url] = {};
		r.promise = new Promise((resolve, _reject) => {
			r.resolve = resolve;
			// r.reject = reject; // We will raise an exception.
		});
	}

	await Promise.all(
		assets.map(async a => {
			a = await a.load();
			let url = a.url;

			for (let r of a.outgoingRelations) {
				if (r.to.url == url) {
					// This works as long as the asset doesn't depend on its own URL.
					continue;
				}

				let dep = dependencies[r.to.url];
				if (!dep) { continue }

				await dep.promise;
			}

			let integrity = null;
			if (sri) {
				let digest = crypto.createHash(sri)
					.update(a.rawSrc)
					.digest("base64");
				integrity = `${sri}-${digest}`;
			}

			if (crossorigin === true) crossorigin = "";

			for (let r of a.incomingRelations) {
				r.hrefType = "absolute";

				if (r.from.type == "Html") {
					if (typeof crossorigin == "string" && CROSSORIGIN_ELEMENTS.test(r.node.tagName))
						r.node.setAttribute("crossorigin", crossorigin);
					if (integrity && INTEGRITY_ELEMENTS.test(r.node.tagName))
						r.node.setAttribute("integrity", integrity);

					if (
						referrerPolicy
						&& REFERRERPOLICY_ELEMENTS.test(r.node.tagName)
						&& !r.node.getAttribute("referrerpolicy")
					)
						r.node.setAttribute("referrerpolicy", referrerPolicy);

					r.from.markDirty();
				}
			}

			let rawSrc = a.rawSrc;

			let path = a.fileName;
			let files = [{
				content: rawSrc,
				path: `d/${path}`,
			}];
			let dir;
			for await (let d of importer(files, null, ipfsOptions)) {
				dir = d;
			}

			let cid = dir.cid;
			let hash = cid.toV0();

			a.url = gatewayUrl({asset: a, cid, path});

			// Also write to the output directory so that it gets uploaded and pinned.
			let local = a.assetGraph.addAsset({
				url: `/ipfs/${cid.toV0()}/${path}`,
				type: "Asset",
				_inferredType: "Asset", // Don't touch our perfect bytes.
				rawSrc,
			});

			if (!rename) {
				let original = a.clone();
				original.url = url;
			}

			dependencies[url].resolve();

			return {
				cid: dir.cid,
				gatewayAsset: a,
				localAsset: local,
			};
		}));
}

export const transformAssets = useGateway;
