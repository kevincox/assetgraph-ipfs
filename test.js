import * as assetgraphIpfs from "./index.js";
import AssetGraph from "assetgraph";
import fs from "fs";
import util from "util";
import {toMatchFilesystemSnapshot} from "jest-fs-snapshot";

expect.extend({ toMatchFilesystemSnapshot });

Buffer.__proto__ = Uint8Array

async function loadSite() {
	let g = new AssetGraph({
		root: import.meta.url + "/../fixtures/basic",
	});
	g.on("info", console.info);
	await g.loadAssets("**/*");
	await g.populate({followRelations: {crossorigin: false}});
	return g;
}

const ROOT_DIR = import.meta.url.replace(/file:\/\/(.*)\/[^\/]*/, "$1");
function snapshotTest(name, f) {
	test(name, async () => {
		let dir = `${ROOT_DIR}/test-out/${name}`;

		try {
			await util.promisify(fs.rm)(dir, {recursive: true});
		} catch (e) {
			if (e.code != 'ENOENT') {
				throw e;
			}
		}

		await f(dir);
		expect(dir).toMatchFilesystemSnapshot();
	});
}

snapshotTest('default', async dir => {
	let g = await loadSite();

	await assetgraphIpfs.useGateway(g.findAssets({
			protocol: "file:",
			path: { $regex: /^\/a\// },
		}));

	await g.writeAssetsToDisc({url: { $regex: /^file:/ }}, dir)
		.writeStatsToStderr();
});

snapshotTest('non-default', async dir => {
	let g = await loadSite();

	await assetgraphIpfs.useGateway(g.findAssets({
			protocol: "file:",
			path: { $regex: /^\/a\// },
		}), {
			crossorigin: null,
			gatewayUrl: ({cid, path}) => `ipfs://${cid.toV1()}/${path}`,
			referrerPolicy: "strict-origin-when-cross-origin",
			sri: null,
		});

	await g.writeAssetsToDisc({url: { $regex: /^file:/ }}, dir)
		.writeStatsToStderr();
});
