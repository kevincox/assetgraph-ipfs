# assetgraph-ipfs

IFPS-related [assetgraph](https://assetgraph.org) tools.

## useGateway

Replaces all assets with links to an IPFS gateway. The assets will also be placed in the output in `/ipfs/{hash}/{fileName}` so that they are properly pinned when the website is.

Warning: If the `ipfsOptions` do not match what you use to add/pin/upload the website the assets may not be accessible by the hash used in the URL. Verify that you are using the correct configuration. This can be done by running `ipfs ls /ipfs/$root_hash/ipfs` and verifying that column 1 (hash) and 3 (name) are the same for each entry.

```javascript
import {useGateway} from "assetgraph-ipfs";

let assetgraph = new AssetGraph(...);

await assetgraphIpfs.useGateway(
	// The assets to modify.
	g.findAssets({path: { $regex: /^\/assets\// }}),
	{
		// By default `crossorigin` attributes are added to references from HTML. There should be no need to send credentials to most IPFS gateways. If your gateway requires credentials pass `false` to disable or a string for the desired value of the `crossorigin` attribute.
		crossorigin: false,

		// The IPFS gateway to use.
		// The current default is shown but subject to change at any time.
		// Note that Cloudflare does not allow video streaming on their gateway.
		gatewayHost = "https://cloudflare-ipfs.com",

		// A function to compute the URL for a given asset.
		//
		// This overrides gatewayhost. It will be passed the assetgraph `Asset`, a cids `CID` and the path inside the CID in which the asset will live. If the CID itself is the asset `path` will be `null`.
		gatewayUrl = ({asset, cid, path}) => `https://ipfs.io/ipfs/${cid.toV0()}/${path}`,

		// Options to use for adding the files to IPFS.
		// See: https://github.com/ipfs/js-ipfs-unixfs/tree/master/packages/ipfs-unixfs-importer
		ipfsOptions: {
			chunker: "rabin",
		},

		// Set the `referrerpolicy` attribute on references to IPFS URLs where supported.
		// See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy#directives for possible values.
		referrerPolicy: "strict-origin-when-cross-origin",

		// By default the assets will be moved to `/ipfs/{hash}`. Set to `false` to leave a copy in their original location.
		rename: false,

		// Disable Subresource Integrity attributes to referencing assets.
		// Since IPFS assets can never change this is enabled by default to remove requirement of trust from the gateway.
		sri: false,
	})
```
