#!/usr/bin/env node

import * as assetgraphIpfs from "./index.js";
import * as fs from "fs";
import AssetGraph from "assetgraph";

process.exitCode = 1;

async function main(_node, _script, output, root, ...entries) {
	let g = new AssetGraph({
		root,
	});
	g.on("info", console.info);
	g.disableFetch = true; // https://github.com/assetgraph/assetgraph/pull/1122
	g.followRelations = {
		crossorigin: false,
	};

	let entry_ids = [];
	for (let path of entries) {
		let a = g.addAsset(encodeURI(`file://${path}`));
		await a.load();
		entry_ids.push(a.id);
	}

	let asset_query = {protocol: "file:", id: {$nin: entry_ids}};
	let entry_query = {id: {$in: entry_ids}};

	await g.populate();

	await assetgraphIpfs.useGateway(g.findAssets(asset_query));

	// console.dir(g.findAssets({fileName: "component.html"}));

	await g.writeAssetsToDisc({protocol: "file:"}, output)
		.writeStatsToStderr();
}

main(...process.argv).then(() => process.exitCode = 0, console.error);
